﻿#include <iostream>

int main()
{
    const int N=3;
    std::cout << "Create an array N*N\n"<<"N = 3\n\n";

    int array[N][N];
    for (int i = 0; i < N; i++)
    {       
        for (int j=0; j < N; j++)
        {           
            array[i][j]=i+j;
            std::cout << array[i][j];
        }
        std::cout << "\n";
    }

    std::cout << "\nCalendar day = ";
    int DAY;
    int sum=0;
    std::cin >> DAY;
 
    for (int i = 0; i < N; i++)
    {
        sum = sum+array[DAY%N][i];
    }
    std::cout << "Sum = " << sum;
}

